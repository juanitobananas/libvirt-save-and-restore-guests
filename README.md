This is just a very simple script to save libvirt (kvm) guests and restore them.

It is very strongly inspired by Kumina's script. Really, it's nearly the same:
https://github.com/kumina/shutdown-kvm-guests

Kudos to Kumina!

# Installation on debian/ubuntu

Just open up a terminal and type the following:

```
wget https://gitlab.com/juanitobananas/libvirt-save-and-restore-guests/raw/master/libvirt-save-and-restore-guests
sudo mv libvirt-save-and-restore-guests /etc/init.d/
sudo chown root:root /etc/init.d/libvirt-save-and-restore-guests
sudo chmod 700 /etc/init.d/libvirt-save-and-restore-guests
sudo update-rc.d libvirt-save-and-restore-guests defaults
```

Tada! Now your machines should 'pause' when your computer shuts down and start
in the same state when you switch it on.

In my case, it didn't work the first time I shut down after installing the
script, but rather the second time.
